# 20230223-GregoryChew-NYCSchools

## Introduction
Programming task to create an app that surfaces the NYC Schools data.

## Notes and Future Enhancements/TODOs
In the interest of expediency, various UI strings are not made into resources as they normally would be.

Dagger2 Dependency Injection can be easily incorporated, and allow for the injection of NetworkManager, SchoolDataManager, SchoolDataList, and SchoolListAdapter in their current respective instantiations.

Using DI would also slightly streamline the visibility declarations for those instances, which are currently defined
as "internal var" for now, in service to the unit tests. Instead, they can be properly set as "private val", and their mock counterparts injected in the unit tests.

The school list is currently maintained as alphabetically sorted. Navigating the list is still not quite ideal, and letter header labels could be added to the list, to delineate the schools beginning with each letter. The adapter can also extend SectionIndexer to aid in navigating the list.

The school data can be stored locally in a persistent-cached fashion, with expiration, so that the app can be used offline.

A refresh mechanism, like a refresh button or pull-to-refresh could be added, so the user can manually update the data.

SchoolDataList can be updated to be a proper AndroidX ViewModel, incorporating the use of LiveData and observers instead of the current direct implementation using SchoolDataListener.

RecyclerView.Adapter could be replaced with ListAdapter, though in this somewhat simplistic case, where the data changes infrequently and there are really only two data update times, this enhancement is not really necessary.
