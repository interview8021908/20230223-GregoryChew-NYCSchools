package com.st1.prog

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.kotlin.*

/**
 * Unit tests for SchoolDataList.
 */
class SchoolDataListTest {
    private val SCHOOL_UDN = "udn"
    private val SCHOOL_NAME = "myschool"

    @Captor
    private var listenerCaptor = argumentCaptor<SchoolDataListener>()

    @Mock
    private lateinit var schoolDataListenerMock: SchoolDataListener

    private lateinit var schoolData: SchoolData

    @Before
    fun initialize() {
        schoolDataListenerMock = mock {}
        schoolData = SchoolData(SCHOOL_UDN, SCHOOL_NAME)
    }

    @Test
    fun simpleRequestResponse() {
        val schoolDataList = SchoolDataList()
        schoolDataList.schoolDataMgr = mock {
            on { getNumSchools() } doReturn 2
        }
        schoolDataList.addListener(schoolDataListenerMock)
        schoolDataList.retrieveData()

        verify(schoolDataList.schoolDataMgr).retrieveData(listenerCaptor.capture())
        listenerCaptor.firstValue.onDataReady()
        verify(schoolDataListenerMock).onDataReady()
        assertEquals(schoolDataList.getNumSchools(), 2)
    }

    @Test
    fun simpleRequestError() {
        val schoolDataList = SchoolDataList()
        schoolDataList.schoolDataMgr = mock {
        }
        schoolDataList.addListener(schoolDataListenerMock)
        schoolDataList.retrieveData()

        verify(schoolDataList.schoolDataMgr).retrieveData(listenerCaptor.capture())
        listenerCaptor.firstValue.onDataError()
        verify(schoolDataListenerMock).onDataError()
        assertEquals(schoolDataList.getNumSchools(), 0)
    }

    @Test
    fun outputSchoolDataPresent() {
        val schoolDataList = SchoolDataList()
        schoolData.satNum = 5
        schoolData.satMath = 300
        schoolData.satReading = 350
        schoolData.satWriting = 400
        schoolDataList.schoolDataMgr = mock {
            on { getSchoolData(any()) } doReturn schoolData
        }

        assertEquals(schoolDataList.getSchoolName(1), SCHOOL_NAME)
        assertEquals(schoolDataList.getSchoolSatTakers(1), "Number of SAT Takers: ${schoolData.satNum}")
        assertEquals(schoolDataList.getSchoolMathSat(1), "Math SAT Score: ${schoolData.satMath}")
        assertEquals(schoolDataList.getSchoolReadingSat(1), "Reading SAT Score: ${schoolData.satReading}")
        assertEquals(schoolDataList.getSchoolWritingSat(1), "Writing SAT Score: ${schoolData.satWriting}")
    }

    @Test
    fun outputSchoolDataAbsent() {
        val schoolDataList = SchoolDataList()
        schoolDataList.schoolDataMgr = mock {
            on { getSchoolData(0) } doReturn schoolData
            on { getSchoolData(1) } doReturn null
        }

        assertEquals(schoolDataList.getSchoolName(0), SCHOOL_NAME)
        assertEquals(schoolDataList.getSchoolSatTakers(0), "SAT Takers Unknown")
        assertEquals(schoolDataList.getSchoolMathSat(0), "Math SAT Unknown")
        assertEquals(schoolDataList.getSchoolReadingSat(0), "Reading SAT Unknown")
        assertEquals(schoolDataList.getSchoolWritingSat(0), "Writing SAT Unknown")

        assertEquals(schoolDataList.getSchoolName(1), "Name Unknown")
        assertEquals(schoolDataList.getSchoolSatTakers(1), "SAT Takers Unknown")
        assertEquals(schoolDataList.getSchoolMathSat(1), "Math SAT Unknown")
        assertEquals(schoolDataList.getSchoolReadingSat(1), "Reading SAT Unknown")
        assertEquals(schoolDataList.getSchoolWritingSat(1), "Writing SAT Unknown")
    }
}