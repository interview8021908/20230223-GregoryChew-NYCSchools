package com.st1.prog

import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONArrayRequestListener
import org.json.JSONArray
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.kotlin.*


/**
 * Unit tests for SchoolDataManager.
 */
class SchoolDataManagerTest {
    private val SCHOOL_JSON = "[{\"$ID_KEY\":\"02M260\",\"$NAME_KEY\":\"School 1\"}," +
            "{\"$ID_KEY\":\"02M261\",\"$NAME_KEY\":\"School 2\"}]"
    private val SCHOOL_JSON_ADD = "[{\"$ID_KEY\":\"02M262\",\"$NAME_KEY\":\"School 3\"," +
            "\"$SATNUM_KEY\":\"5\",\"$SATMATH_KEY\":\"300\",\"$SATREADING_KEY\":\"350\"," +
            "\"$SATWRITING_KEY\":\"400\"}]"
    private val SCHOOL_JSON_MODIFY = "[{\"dbn\":\"02M261\",\"school_name\":\"School 2\"," +
            "\"$SATNUM_KEY\":\"5\",\"$SATMATH_KEY\":\"300\",\"$SATREADING_KEY\":\"350\"," +
            "\"$SATWRITING_KEY\":\"400\"}]"

    @Captor
    private val listenerCaptor = ArgumentCaptor.forClass(JSONArrayRequestListener::class.java)

    @Mock
    private lateinit var schoolDataListenerMock: SchoolDataListener

    @Before
    fun initialize() {
        schoolDataListenerMock = mock {}
    }

    @Test
    fun simpleJSONResponseParsing() {
        val schoolDataManager = SchoolDataManager()
        schoolDataManager.netMgr =  mock {
        }
        schoolDataManager.retrieveData(schoolDataListenerMock)

        verify(schoolDataManager.netMgr).getSATData(eq(MAINLIST_URL), listenerCaptor.capture())
        listenerCaptor.value.onResponse(JSONArray(SCHOOL_JSON))
        verify(schoolDataListenerMock).onDataReady()

        assertEquals(schoolDataManager.getNumSchools(), 2)
        assertEquals(schoolDataManager.getSchoolData(0)?.satNum, -1)
        assertEquals(schoolDataManager.getSchoolData(1)?.satNum, -1)
        assertEquals(schoolDataManager.getSchoolData(0)?.satMath, -1)
        assertEquals(schoolDataManager.getSchoolData(1)?.satMath, -1)
        assertEquals(schoolDataManager.getSchoolData(0)?.satReading, -1)
        assertEquals(schoolDataManager.getSchoolData(1)?.satReading, -1)
        assertEquals(schoolDataManager.getSchoolData(0)?.satWriting, -1)
        assertEquals(schoolDataManager.getSchoolData(1)?.satWriting, -1)
    }

    @Test
    fun simpleJSONResponseError() {
        val schoolDataManager = SchoolDataManager()
        schoolDataManager.netMgr =  mock {
        }
        schoolDataManager.retrieveData(schoolDataListenerMock)

        verify(schoolDataManager.netMgr).getSATData(eq(MAINLIST_URL), listenerCaptor.capture())
        listenerCaptor.value.onError(ANError())
        verify(schoolDataListenerMock).onDataError()

        assertEquals(schoolDataManager.getNumSchools(), 0)
    }

    @Test
    fun addSATResponseParsing() {
        val schoolDataManager = SchoolDataManager()
        schoolDataManager.netMgr =  mock {
        }
        schoolDataManager.retrieveData(schoolDataListenerMock)

        verify(schoolDataManager.netMgr).getSATData(eq(MAINLIST_URL), listenerCaptor.capture())
        listenerCaptor.value.onResponse(JSONArray(SCHOOL_JSON))

        verify(schoolDataManager.netMgr).getSATData(eq(SATLIST_URL), listenerCaptor.capture())
        listenerCaptor.value.onResponse(JSONArray(SCHOOL_JSON_ADD))
        verify(schoolDataListenerMock, times(2)).onDataReady()

        assertEquals(schoolDataManager.getNumSchools(), 3)
        assertEquals(schoolDataManager.getSchoolData(0)?.satNum, -1)
        assertEquals(schoolDataManager.getSchoolData(1)?.satNum, -1)
        assertEquals(schoolDataManager.getSchoolData(2)?.satNum, 5)
        assertEquals(schoolDataManager.getSchoolData(2)?.satMath, 300)
        assertEquals(schoolDataManager.getSchoolData(2)?.satReading, 350)
        assertEquals(schoolDataManager.getSchoolData(2)?.satWriting, 400)
    }

    @Test
    fun updateSATResponseParsing() {
        val schoolDataManager = SchoolDataManager()
        schoolDataManager.netMgr =  mock {
        }
        schoolDataManager.retrieveData(schoolDataListenerMock)

        verify(schoolDataManager.netMgr).getSATData(eq(MAINLIST_URL), listenerCaptor.capture())
        listenerCaptor.value.onResponse(JSONArray(SCHOOL_JSON))

        verify(schoolDataManager.netMgr).getSATData(eq(SATLIST_URL), listenerCaptor.capture())
        listenerCaptor.value.onResponse(JSONArray(SCHOOL_JSON_MODIFY))
        verify(schoolDataListenerMock, times(1)).onDataReady()

        assertEquals(schoolDataManager.getNumSchools(), 2)
        assertEquals(schoolDataManager.getSchoolData(0)?.satNum, -1)
        assertEquals(schoolDataManager.getSchoolData(1)?.satNum, 5)
        assertEquals(schoolDataManager.getSchoolData(1)?.satMath, 300)
        assertEquals(schoolDataManager.getSchoolData(1)?.satReading, 350)
        assertEquals(schoolDataManager.getSchoolData(1)?.satWriting, 400)
    }
}