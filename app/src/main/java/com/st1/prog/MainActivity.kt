package com.st1.prog

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private val schoolDataList: SchoolDataList = SchoolDataList()
    private val schoolListAdapter: SchoolListAdapter = SchoolListAdapter(this, schoolDataList)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView = findViewById<RecyclerView>(R.id.schoolList)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(baseContext)
        recyclerView.adapter = schoolListAdapter

        schoolDataList.addListener(object: SchoolDataListener {
            override fun onDataReady() {
                updateSchoolViews()
            }

            override fun onDataError() {
                displayError("Error retrieving data.")
            }
        })
        schoolDataList.retrieveData()
    }

    /**
     * Act on notification of updated school data
     */
    private fun updateSchoolViews() {
        schoolListAdapter.notifyDataSetChanged()
        // Check if the refreshed list is not empty.
        if (schoolDataList.getNumSchools() <= 0) {
            findViewById<RecyclerView>(R.id.schoolList).visibility = View.INVISIBLE
            displayError("No School Data available")
        } else {
            findViewById<RecyclerView>(R.id.schoolList).visibility = View.VISIBLE
            findViewById<TextView>(R.id.statusText).visibility = View.INVISIBLE
        }
    }

    /**
     * Update the status text with an error message.
     */
    private fun displayError(errorMessage: String) {
        findViewById<TextView>(R.id.statusText).let {
            it.visibility = View.VISIBLE
            it.text = errorMessage
        }
    }
}