package com.st1.prog

import com.androidnetworking.common.ANRequest
import com.androidnetworking.interfaces.JSONArrayRequestListener
import com.androidnetworking.AndroidNetworking

/**
 * Simple abstraction of networking layer
 */
class NetworkManager {
    private var currRequest: ANRequest<*>? = null

    fun getSATData(url: String, requestListener: JSONArrayRequestListener?) {
        currRequest = AndroidNetworking.get(url)
            .build()
        currRequest?.getAsJSONArray(requestListener)
    }

    fun cancel() {
        currRequest?.cancel(true)
    }
}