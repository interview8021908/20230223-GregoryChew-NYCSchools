package com.st1.prog

/**
 * Format Schools data for the UI.
 */
class SchoolDataList {
    internal var schoolDataMgr: SchoolDataManager = SchoolDataManager()
    private val listeners: ArrayList<SchoolDataListener> = ArrayList()

    fun retrieveData() {
        schoolDataMgr.retrieveData(object: SchoolDataListener {
            override fun onDataReady() {
                notifyListeners(true)
            }

            override fun onDataError() {
                notifyListeners(false)
            }
        })
    }

    fun addListener(listener: SchoolDataListener) {
        listeners.add(listener)
    }

    fun removeListener(listener: SchoolDataListener) {
        listeners.remove(listener)
    }

    fun getNumSchools() : Int {
        return schoolDataMgr.getNumSchools()
    }

    fun getSchoolName(position: Int) : String {
        return schoolDataMgr.getSchoolData(position)?.name ?: "Name Unknown"
    }

    fun getSchoolSatTakers(position: Int) : String {
        val numSat = schoolDataMgr.getSchoolData(position)?.satNum ?: -1
        return (if (numSat >= 0) "Number of SAT Takers: $numSat" else "SAT Takers Unknown")
    }

    fun getSchoolMathSat(position: Int) : String {
        val mathSat = schoolDataMgr.getSchoolData(position)?.satMath ?: -1
        return (if (mathSat >= 0) "Math SAT Score: $mathSat" else "Math SAT Unknown")
    }

    fun getSchoolWritingSat(position: Int) : String {
        val writeSat = schoolDataMgr.getSchoolData(position)?.satWriting ?: -1
        return (if (writeSat >= 0) "Writing SAT Score: $writeSat" else "Writing SAT Unknown")
    }

    fun getSchoolReadingSat(position: Int) : String {
        val readSat = schoolDataMgr.getSchoolData(position)?.satReading ?: -1
        return (if (readSat >= 0) "Reading SAT Score: $readSat" else "Reading SAT Unknown")
    }

    private fun notifyListeners(isSuccess: Boolean) {
        listeners.forEach { listener ->
            if (isSuccess) listener.onDataReady() else listener.onDataError()
        }
    }

}