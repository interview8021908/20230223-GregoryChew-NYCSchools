package com.st1.prog

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView

/**
 * RecyclerView Adapter for the list of schools.
 */
class SchoolListAdapter(private val context: Context, private val schoolDataList: SchoolDataList) : RecyclerView.Adapter<SchoolListAdapter.SchoolItemVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolItemVH {
        return SchoolItemVH(LayoutInflater.from(parent.context).inflate(R.layout.school_item, null, false))
    }

    override fun onBindViewHolder(holder: SchoolItemVH, position: Int) {
        holder.setName(schoolDataList.getSchoolName(position))
        holder.itemView.setOnClickListener { _ ->
            buildDialogForPosition(position)
        }
    }

    override fun getItemCount(): Int {
        return schoolDataList.getNumSchools()
    }

    /**
     * Create the dialog showing the school's SAT information.
     */
    private fun buildDialogForPosition(position: Int) {
        AlertDialog.Builder(context)
            .setTitle(schoolDataList.getSchoolName(position))
            .setMessage(schoolDataList.getSchoolSatTakers(position) + "\n"
                    + schoolDataList.getSchoolMathSat(position) + "\n"
                    + schoolDataList.getSchoolReadingSat(position) + "\n"
                    + schoolDataList.getSchoolWritingSat(position) + "\n")
            .setNeutralButton("OK") { _, _ -> }
            .show()
    }

    inner class SchoolItemVH(schoolItemView: View) : RecyclerView.ViewHolder(schoolItemView) {
        private val nameView = schoolItemView.findViewById<TextView>(R.id.school_name)

        fun setName(name: String) {
            nameView.text = name
        }
    }

}