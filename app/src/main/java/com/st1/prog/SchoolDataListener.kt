package com.st1.prog

interface SchoolDataListener {
    fun onDataReady()
    fun onDataError()
}