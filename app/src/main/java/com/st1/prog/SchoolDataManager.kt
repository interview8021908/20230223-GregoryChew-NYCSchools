package com.st1.prog

import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONArrayRequestListener
import org.json.JSONArray
import org.json.JSONObject

const val MAINLIST_URL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
const val SATLIST_URL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
const val ID_KEY = "dbn"
const val NAME_KEY = "school_name"
const val SATNUM_KEY = "num_of_sat_test_takers"
const val SATMATH_KEY = "sat_math_avg_score"
const val SATREADING_KEY = "sat_critical_reading_avg_score"
const val SATWRITING_KEY = "sat_writing_avg_score"

/**
 * Manage the internal cache of the list of schools.
 */
class SchoolDataManager {
    internal var netMgr : NetworkManager = NetworkManager()
    private var schoolList: ArrayList<SchoolData> = ArrayList()
    private val schoolMap: HashMap<String, SchoolData> = HashMap()

    /**
     * Retrieve basic schools list data
     */
    fun retrieveData(listener: SchoolDataListener) {
        netMgr.getSATData(MAINLIST_URL, object: JSONArrayRequestListener {
            override fun onResponse(response: JSONArray?) {
                if (response != null) {
                    // Parse and store the school data, alert listener when its ready, then start second-phase.
                    setSchoolData(response)
                    listener.onDataReady()
                    getAdditionalData(listener)
                }
            }

            override fun onError(anError: ANError?) {
                listener.onDataError()
            }
        })
    }

    fun getNumSchools() : Int {
        return schoolList.size
    }

    fun getSchoolData(index: Int) : SchoolData? {
        return try {
            schoolList[index]
        } catch (iobe: IndexOutOfBoundsException) {
            null
        }
    }

    /**
     * Update the internal schools list and sort it.
     */
    private fun setSchoolData(rawData: JSONArray) {
        val lastIndex = rawData.length() - 1

        //Got some results, clear the internal store
        if (lastIndex >= 0) {
            schoolList.clear()
            schoolMap.clear()
        }
        for (index in 0..lastIndex) {
            val schoolObj: JSONObject? = rawData.optJSONObject(index)
            if (schoolObj != null) {
                val schoolId: String = schoolObj.optString(ID_KEY, "")
                if (schoolId.isNotBlank()) {
                    val school = SchoolData(schoolId, schoolObj.optString(NAME_KEY, "Name Unknown"))
                    schoolList.add(school)
                    schoolMap[schoolId] = school
                }
            }
        }
        schoolList.sort()
    }

    /**
     * Retrieve SAT data for schools.
     */
    private fun getAdditionalData(listener: SchoolDataListener) {
        netMgr.getSATData(SATLIST_URL, object: JSONArrayRequestListener {
            override fun onResponse(response: JSONArray?) {
                if (response != null) {
                    if (augmentSchoolData(response)) {
                        listener.onDataReady()
                    }
                }
            }

            override fun onError(anError: ANError?) {
                //Do nothing, SAT scores simply do not get filled in.
            }
        })
    }

    /**
     * Populate SAT sores of schools, adding new schools to the existing list if necessary.
     *
     * Return true if new schools have been added, so listener can be notified.
     */
    private fun augmentSchoolData(augData: JSONArray): Boolean {
        val lastIndex = augData.length() - 1
        var addedSchools = false

        for (index in 0..lastIndex) {
            val schoolObj: JSONObject? = augData.optJSONObject(index)
            if (schoolObj != null) {
                val schoolId: String = schoolObj.optString(ID_KEY, "")
                if (schoolId.isNotBlank()) {
                    val school: SchoolData = schoolMap[schoolId] ?: SchoolData(
                        schoolId,
                        schoolObj.optString(NAME_KEY, "Name Unknown")
                    )
                    school.satNum = schoolObj.optInt(SATNUM_KEY, -1)
                    school.satMath = schoolObj.optInt(SATMATH_KEY, -1)
                    school.satReading = schoolObj.optInt(SATREADING_KEY, -1)
                    school.satWriting = schoolObj.optInt(SATWRITING_KEY, -1)

                    //Add a new school entry if it doesn't exist
                    if (!schoolMap.containsKey(schoolId)) {
                        schoolMap[schoolId] = school
                        schoolList.add(school)
                        addedSchools = true
                    }
                }
            }
        }

        if (addedSchools) {
            schoolList.sort()
        }

        return addedSchools
    }

}