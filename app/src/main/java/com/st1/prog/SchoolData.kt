package com.st1.prog

/**
 * Basic store for the School Data, with minimum construction for udn and school name.
 */
data class SchoolData(val udn: String, val name: String): Comparable<SchoolData> {
    var satNum: Int = -1
    var satMath: Int = -1
    var satReading: Int = -1
    var satWriting: Int = -1

    override fun compareTo(other: SchoolData): Int {
        return this.name.compareTo(other.name)
    }
}